int porta_rele = 2;
//RFID
#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          7        // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;
//====================
//ETHERNET

#include <Ethernet.h>
char idRfidServidor[15] = {};
// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
IPAddress server(169, 254, 227, 207);  // numeric IP for Google (no DNS)
//char server[] = "www.google.com";    // name address for Google (using DNS)

// Set the static IP address to use if the DHCP fails to assign
IPAddress ip(169, 254, 227, 209); // IP COM O ULTIMO DIFERENTE

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

//=====================
void setup() {
  Serial.begin(9600);
  pinMode(porta_rele, OUTPUT);
  digitalWrite(porta_rele, HIGH);//Desliga rele 1
  //RFID
  SPI.begin();        // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card

  // Prepare the key (used both as key A and as key B)
  // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }

  Serial.println(F("Scan a MIFARE Classic PICC to demonstrate read and write."));
  Serial.print(F("Using key (for A and B):"));
  dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
  Serial.println();

  Serial.println(F("BEWARE: Data will be written to the PICC, in sector #1"));
  //===================
  // ETHERNET
  Ethernet.begin(mac, ip);
  /*if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip);
    }/*
    //=================
    /*((client.connect(server, 80)));
    delay(1000);
    if(client.connected()){
    client.print("GET /TCCFELLIPE/fellipe/nome%20do%20projeto/Visao/Registra.php?key=");
    client.print("763276216732");
    client.println(" HTTP/1.1");
    client.println("Host: 169.254.227.207 ");
    client.println("Connection: close");
    client.println();
    }*/

  Serial.println("Informe Cartao");
}

void loop() {
  // RFID
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
    return;
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
    return;

  Serial.print(F("Card UID:"));
  String UID = dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
  Serial.println(UID);
  manda_dados(UID);
  String verificaAbriu = readPage();
  Serial.print("resultado: ");
  Serial.println(verificaAbriu);
  if (verificaAbriu.equals("1")) {
    Serial.println("Bem-Vindo!");
    digitalWrite(porta_rele, LOW);
    delay(10000);
    digitalWrite(porta_rele, HIGH);//Desliga rele 1
  } else {
    Serial.println("Acesso Negado");
    digitalWrite(porta_rele, HIGH); //Desliga rele 1
  }
  Serial.println();
  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
  // ================
}

String dump_byte_array(byte *buffer, byte bufferSize) {
  String retorno = "";
  for (byte i = 0; i < bufferSize; i++) {
    //Serial.print((buffer[i] < 0x10 ? " 0" : " "));
    retorno.concat(String(buffer[i] < 0x10 ? " 0" : " "));
    //Serial.print(buffer[i], HEX);
    retorno.concat(String(buffer[i], HEX));
  }
  retorno.trim();
  return retorno;
}

void manda_dados(String codigoRfid) {
  codigoRfid.replace(" ", "%20"); //espaço no codigo RFID vira %20
  // if you get a connection, report back via serial:
  ((client.connect(server, 80)));
  delay(2000); //Esperar estabelecer conexão

  //if (client.connect(server, 80)) {
  if (client.connected()) { //verifica se conexao foi estabelecida corretamente
    Serial.println("connected");
    // Make a HTTP request:
    client.print("GET /TCCFELLIPE/fellipe/nome%20do%20projeto/Visao/Registra.php?key=");
    client.print(codigoRfid);
    client.println(" HTTP/1.1");
    client.println("Host: 169.254.227.207 ");
    client.println("Connection: close");
    client.println();
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

String readPage() {
  //read the page, and capture & return everything between '<' and '>'
  String inString = "";
  boolean startRead = false;
  // memset( &inString, 0, 32 ); //clear inString memory

  while (true) {

    if (client.available()) {
      char c = client.read();

      if (c == '<' ) { //'<' is our begining character
        startRead = true; //Ready to start reading the part
      } else if (startRead) {

        if (c != '>') { //'>' is our ending character
          inString.concat(c);
        } else {
          //got what we need here! We can disconnect now
          startRead = false;
          client.stop();
          client.flush();
          Serial.println("disconnecting.");
          return inString;

        }
      }
    }
  }
}

