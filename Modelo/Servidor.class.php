<?php

include_once '../Controle/Conexao.controller.php';

class Servidor {

    private $id;
    private $nome;
    private $matricula;
    private $codigo;
    private $cadastrado_por;

    function __construct($nome, $matricula, $codigo, $cadastrado_por) {
        $this->nome = $nome;
        $this->matricula = $matricula;
        $this->codigo = $codigo;
        $this->cadastrado_por = $cadastrado_por;
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getMatricula() {
        return $this->matricula;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getCadastrado_por() {
        return $this->cadastrado_por;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setMatricula($matricula) {
        $this->matricula = $matricula;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setCadastrado_por($cadastrado_por) {
        $this->cadastrado_por = $cadastrado_por;
    }

    public function salvar() {
        try {

            $conexao = Conexao::conectar();

            $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conexao->prepare('INSERT INTO servidor (nm_servidor,id_matricula,codigo_rfid,cadastrado_por) VALUES (?,?,?,?)');
            $stmt->bindParam(1, $this->getNome());
            $stmt->bindParam(2, $this->getMatricula());
            $stmt->bindParam(3, $this->getCodigo());
            $stmt->bindParam(4, $this->getCadastrado_por());

            $stmt->execute();
            header('location: ../Visao/ServidorCriar.view.php');
        } catch (PDOException $e) {
            header('location: ../Visao/ServidorCriar.view.php?erros=true');
            exit;
        }
    }

    public function alterar($matricula) {
        try {
            $conexao = Conexao::conectar();
            $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conexao->prepare('UPDATE SERVIDOR SET nm_servidor=?, id_matricula=?,codigo_rfid=?,cadastrado_por=? WHERE ID_MATRICULA = ?');
            $stmt->bindParam(1, $this->getNome());
            $stmt->bindParam(2, $this->getMatricula());
            $stmt->bindParam(3, $this->getCodigo());
            $stmt->bindParam(4, $this->getCadastrado_por());
            $stmt->bindParam(5, $matricula);

            $stmt->execute();
            header('location: ../Visao/ServidorInicio.view.php');
        } catch (PDOException $e) {
            header('location: ../Visao/ServidorInicio.view.php?erro=true');
            exit;
        }
//        $this->nome = $row->nome;
//        $this->matricula = $row->matricula;
//        return $this;
    }

    public function buscar($matricula) {
        $conexao = Conexao::conectar();
        $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = 'SELECT * FROM SERVIDOR WHERE ID_MATRICULA = ' . $matricula;
        $row = $conexao->query($query);

        //$stmt->bindParam(1, $matricula);
        //$stmt->execute();
        $aux = $row->fetch(PDO::FETCH_ASSOC);
        $this->nome = $aux['nm_servidor'];
        $this->codigo = $aux['codigo_rfid'];
        $this->matricula = $aux['id_matricula'];
        return $this;
    }

    public function excluir() {
        $conexao = Conexao::conectar();
        $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conexao->prepare('UPDATE SERVIDOR SET FL_EXCLUIDO = TRUE WHERE ID_MATRICULA = ?');
        $stmt->bindParam(1, $this->matricula);
        $stmt->execute();
        return true;
    }

    public function listarTudo() {
        $conexao = Conexao::conectar();
        $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $servidores = $conexao->query('SELECT nm_servidor as nome,codigo_rfid as codigo,id_matricula as matricula FROM SERVIDOR WHERE FL_EXCLUIDO =FALSE');
        while ($servidor = $servidores->fetch(PDO::FETCH_ASSOC)) {
            echo'<tr>
                    <td>' . $servidor['nome'] . '</td>
                    <td>' . $servidor['codigo'] . '</td>
                    <td> ' . $servidor['matricula'] . '</td>
                    <td>
                        <a href="../Controle/Controle.php?rota=ServidorControle@editar&matricula=' . $servidor['matricula'] . '" class="btn btn-primary"><i class="fa fa-btn fa-pencil fa-fw" aria-hidden="true"></i>Editar</a>
                        <a href="../Controle/Controle.php?rota=ServidorControle@deletar&matricula=' . $servidor['matricula'] . '" class="btn btn-danger"><i class="fa fa-btn fa-trash fa-fw" aria-hidden="true"></i>Excluir</a>
                    </td>
                </tr>';
        }
    }

}
