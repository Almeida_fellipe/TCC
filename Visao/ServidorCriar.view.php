<?php
session_start();
?>  
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Compartment Security</title>
        <link href="../Bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style>
            * {
                font-family: 'Roboto', sans-serif !important;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default" style="background-color: rgb(52,154,70);">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="color: white;">Compartment Security</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    
                    <?php
                    if (isset($_SESSION['matricula_usuario'])) {
                        $nome = '0' . unserialize($_SESSION['matricula_usuario']);
                        ?>
                        <ul class="nav navbar-nav">
                            <li><a href="ServidorInicio.view.php" style="color: white;">Início</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right" >

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"style="color: white;"> <?php echo $nome ?> <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../Controle/Controle.php?rota=LoginControle@logout">Sair</a></li>
                                </ul>
                            </li>
                        </ul>
                        <?php
                    }
                    ?>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-md-offset-3 col-lg-5 col-lg-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Cadastro de Servidor</div>
                        <?php
                        if (isset($_GET['erros'])) {
                            ?>   
                            <div class="panel-body">
                                <div class="alert alert-danger" role="alert">RFID e Matrícula já existentes!</div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="panel-body">
                            <form action="../Controle/Controle.php?rota=ServidorControle@salvar" method="post">
                                <div class="form-group">
                                    <label for="nome">Nome do Servidor:</label>
                                    <input type="text" class="form-control" id="nome" name="nome">
                                </div>
                                <div class="form-group">
                                    <label for="matricula">Matrícula:</label>
                                    <input type="text" class="form-control" id="matricula" name="matricula">
                                </div>
                                <div class="form-group">
                                    <label for="codigo">RFID:</label>
                                    <input type="text" class="form-control" id="codigo" name="codigo">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default pull-right" style = "margin-top: 5px;">Salvar</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../Bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>


