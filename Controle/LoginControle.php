<?php
session_start();
include_once './Conexao.controller.php';


class LoginControle {

    public function login($dados) {
        $conexao = Conexao::conectar();
        $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $stmt = $conexao->prepare('SELECT matricula_usuario FROM Usuario WHERE matricula_usuario = ? AND senha_usuario = ?');
        $stmt->bindParam(1, $dados['matricula']);
        $stmt->bindParam(2, md5($dados['senha']));
        $stmt->execute();
        $rst = $stmt->fetch(PDO::FETCH_OBJ);
        print_r($rst);
        if (isset($rst->matricula_usuario)) {
            $_SESSION['matricula_usuario'] = serialize($rst->matricula_usuario);
            header('location: ../Visao/ServidorInicio.view.php');
        } else {
            header('location: ../Visao/Login.view.php?erros=true');
        }
    }

    public function logout() {
        session_destroy();
        header('location: ../Visao/Login.view.php');
    }

}
