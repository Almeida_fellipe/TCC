<?php

$rota = filter_input(INPUT_GET, 'rota', FILTER_SANITIZE_STRING); //O QUE TA PASSANDO POR GET
$get = filter_input_array(INPUT_GET);
$dados = filter_input_array(INPUT_POST);

$parametros = explode('@', $rota);
$controlador = $parametros[0];
$acao = $parametros[1];

include_once $controlador . '.php';

$controle = new $controlador();
$dados ? $controle->$acao($dados, $get) : $controle->$acao($get);
exit();

