<?php

session_start();
include_once '../Modelo/Servidor.class.php';

class ServidorControle {

    public function inicio() {
        
    }

    public function criar() {
        
    }

    public function salvar($dados) {
        $user = unserialize($_SESSION['matricula_usuario']);
        $servidor = new Servidor($dados['nome'], $dados['matricula'], $dados['codigo'], $user);
        $servidor->salvar();
        header('location: ../Visao/ServidorInicio.view.php');
        die('Sucesso');
    }

    public function editar($dados) {
        header('location: ../Visao/ServidorEditar.view.php?id=' . $dados['matricula']);
    }

    public function atualizar($dados) {
        //var_dump($dados);
        $user = unserialize($_SESSION['matricula_usuario']);
        $servidor = new Servidor($dados['nome'], $dados['matricula'], $dados['codigo'], $user);
        $servidor->alterar($dados['matricula']);
        die('Sucesso');
    }

    public function deletar($dados) {
          $servidor = new Servidor(null,$dados['matricula'],null,null);
          $servidor->excluir();
          header('location: ../Visao/ServidorInicio.view.php?id=' . $dados['matricula']);
    }

}
